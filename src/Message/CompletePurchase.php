<?php

namespace Omnipay\Paymob\Message;

use Omnipay\Common\Message\AbstractRequest;
use Paymob\Library\Paymob;

class CompletePurchase extends AbstractRequest
{

    public function setHmac($value)
    {
        return $this->setParameter('Hmac', $value);
    }
    /**
     * Get the gateway Hmac
     *
     * @return string
     */
    public function getHmac()
    {
        return $this->getParameter('Hmac');
    }
    public function getData()
    {

        return array('amount' => $this->getAmount(), 'data' => $_GET);
    }
    public function sendData($data)
    {
       
        $data['reference'] = $_GET['order'];
        if (!Paymob::verifyHmac($this->getHmac(), $_GET)) {
            throw new \Exception('Ops, you are accessing wrong data');
        }
        if (
            Paymob::filterVar('success') === "true" &&
            Paymob::filterVar('is_voided') === "false" &&
            Paymob::filterVar('is_refunded') === "false"
        ) {
            $data['success'] = 1;
            $data['message'] = 'Paymob : Payment Approved';

        } else {
            $data['success'] = 0;
            $data['message'] = 'Paymob : Payment is not completed';
        }
        return $this->response = new Response($this, $data);
    }
}