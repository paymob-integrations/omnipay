<?php

/**
 * Paymob Gateway
 */

namespace Omnipay\Paymob;

use Omnipay\Common\AbstractGateway;
use Paymob\Library\Paymob;

class Gateway extends AbstractGateway
{
    public function getName()
    {
        return 'Paymob';
    }

    /**
     * Get default parameters for this gateway
     *
     * @return void
     */
    public function getDefaultParameters()
    {
        return array(
            'apiKey' => '',
            'SecretKey' => '',
            'PublicKey' => '',
            'IntegrationId' => '',
            'Hmac' => ''
        );
    }

    /**
     * Get the gateway apiKey key
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->getParameter('apiKey');
    }

    /**
     * Set the gateway apiKey key
     *
     * @param  string $value
     * @return Gateway provides a fluent interface.
     */
    public function setApiKey($value)
    {
        return $this->setParameter('apiKey', $value);
    }

    /**
     * Get the gateway SecretKey
     *
     * @return string
     */
    public function getSecretKey()
    {
        return $this->getParameter('SecretKey');
    }

    /**
     * @param  string $value
     * @return $this
     */
    public function setSecretKey($value)
    {
        return $this->setParameter('SecretKey', $value);
    }
    /**
     * Get the gateway PublicKey
     *
     * @return string
     */
    public function getPublicKey()
    {
        return $this->getParameter('PublicKey');
    }

    /**
     * @param  string $value
     * @return $this
     */
    public function setPublicKey($value)
    {
        return $this->setParameter('PublicKey', $value);
    }

    /**
     * @param  string $value
     * @return $this
     */
    public function setIntegrationId($value)
    {
        return $this->setParameter('IntegrationId', $value);
    }
    /**
     * Get the gateway PublicKey
     *
     * @return string
     */
    public function getIntegrationId()
    {
        return $this->getParameter('IntegrationId');
    }
    /**
     * @param  string $value
     * @return $this
     */
    public function setHmac($value)
    {
        return $this->setParameter('Hmac', $value);
    }
    /**
     * Get the gateway Hmac
     *
     * @return string
     */
    public function getHmac()
    {
        return $this->getParameter('Hmac');
    }

    /**
     * purchase Request
     *
     *
     * @param  array|array $parameters
     * @return \Omnipay\Paymob\Message\Response
     */
    public function purchase(array $parameters = array())
    {
        $secretkey = $this->getSecretKey();
        $publickey = $this->getPublicKey();
        $integrationIds = $this->getIntegrationId();
        $integrations = explode(',', $integrationIds);
        $integration_ids = [];
        foreach ($integrations as $id) {
            $id = (int) $id;
            if ($id > 0) {
                array_push($integration_ids, $id);
            }
        }
        $billing = [
            "email" => $parameters['card']['email'],
            "first_name" => $parameters['card']['firstName'],
            "last_name" => $parameters['card']['lastName'],
            "street" => !empty($parameters['card']['address1']) ? $parameters['card']['address1'] . ' - ' . $parameters['card']['address2'] : 'NA',
            "phone_number" => !empty($parameters['card']['phone']) ? $parameters['card']['phone'] : 'NA',
            "city" => !empty($parameters['card']['city']) ? $parameters['card']['city'] : 'NA',
            "country" => !empty($parameters['card']['country']) ? $parameters['card']['country'] : 'NA',
            "state" => !empty($parameters['card']['state']) ? $parameters['card']['state'] : 'NA',
            "postal_code" => !empty($parameters['card']['postcode']) ? $parameters['card']['postcode'] : 'NA',
        ];
        $orderId = $parameters['transactionId'];
        $currency = $parameters['currency'];
        $price = $parameters['amount'];
        $country = Paymob::getCountryCode($secretkey);
        $cents = 100;
        $round = 2;
        if ($country == 'omn') {
            $cents = 1000;
        }

        $price = round((round($price, $round)) * $cents, $round);

        $data = [
            "amount" => $price,
            "currency" => $currency,
            "payment_methods" => $integration_ids,
            "billing_data" => $billing,
            "extras" => ["merchant_intention_id" => $orderId . '_' . time()],
            "special_reference" => $orderId . '_' . time()
        ];
        $paymobReq = new Paymob('', '');
        $status = $paymobReq->createIntention($secretkey, $data, $orderId);
        if (!$status['success']) {
            throw new \Exception($status['message']);
        } else {

            $countryCode = $paymobReq->getCountryCode($secretkey);
            $apiUrl = $paymobReq->getApiUrl($countryCode);
            $cs = $status['cs'];
            $url = $apiUrl . "unifiedcheckout/?publicKey=$publickey&clientSecret=$cs";
            header("Location:" . $url);
            exit;
        }
    }

    /**
     * completePurchase Request
     *
     *
     * @param  array|array $parameters
     * @return \Omnipay\Paymob\Message\Response
     */
    public function completePurchase(array $parameters = array())
    {
        return $this->createRequest('\Omnipay\Paymob\Message\CompletePurchase', $parameters);
    }
}