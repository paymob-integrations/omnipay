# Paymob Payment for Omnipay

This package implements Paymob Payment for [Omnipay](https://github.com/thephpleague/omnipay), the multi-gateway payment
processing library. 

## Installation

```
composer require paymob/omnipay
```

## Configuration
### Paymob Account
1. Login to the Paymob account → Setting in the left menu. 
2. Get the Secret, public, API keys, HMAC and Payment Methods IDs (integration IDs).

### Merchant Configurations
1. Edit the gateway setting and paste each key in its place.
2. Please ensure adding the integration IDs separated by comma ,. These IDs will be shown in the Paymob payment page. 
3. Copy integration callback URL,replace only the {YourWebsiteURL} with your site domain. Then, paste it into each payment integration/method in Paymob account.

> https://{YourWebsiteURL}/gateways/Paymob/completePurchase

4. Below URL is considered as your website payment process for Paymob Payment. Just replace the {YourWebsiteURL} with the actual domain.

> https://{YourWebsiteURL}/gateways/Paymob/purchase
